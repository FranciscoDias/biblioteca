<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Autores;
use App\Models\Livros;
use App\Models\Editoras;

class SystemController extends Controller
{
    public function lislivros(){
        $livros=Livros::all();
        return view('listagemLivros', ['livros'=>$livros]);
    }
}
